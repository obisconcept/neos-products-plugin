# Neos CMS - Product catalog plugin
A small node type based product catalog plugin for Neos CMS.

## Installation
Add the package in your site package composer.json
```
"require": {
    "obisconcept/neos-products-plugin": "~1.1"
}
```

## Version History

### Changes in 1.1.0
- Changed category and product overview and added new product teaser nodetype

### Changes in 1.0.0
- First version of the Neos CMS product catalog plugin