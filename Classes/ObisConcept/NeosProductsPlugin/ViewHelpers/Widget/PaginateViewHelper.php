<?php namespace ObisConcept\NeosProductsPlugin\ViewHelpers\Widget;

use Neos\Flow\Annotations as Flow;
use Neos\ContentRepository\Domain\Model\NodeInterface;
use Neos\ContentRepository\ViewHelpers\Widget\PaginateViewHelper as ContentRepositoryPaginateViewHelper;

class PaginateViewHelper extends ContentRepositoryPaginateViewHelper
{

    /**
     * @Flow\Inject
     * @var \ObisConcept\NeosProductsPlugin\ViewHelpers\Widget\Controller\PaginateController
     */
    protected $controller;
}
