<?php namespace ObisConcept\NeosProductsPlugin\TypoScript\Eel\FlowQueryOperations;

/*
 * Class by Lelesys.News
 * https://github.com/lelesys/Lelesys.News/blob/master/Classes/Lelesys/News/TypoScript/Eel/FlowQueryOperations/FilterOperation.php
 */

use Neos\Flow\Annotations as Flow;
use Neos\ContentRepository\Domain\Model\NodeInterface;

class FilterOperation extends \Neos\ContentRepository\Eel\FlowQueryOperations\FilterOperation
{

    /**
     * {@inheritdoc}
     *
     * @var integer
     */
    protected static $priority = 200;

    /**
     * {@inheritdoc}
     *
     * @param array (or array-like object) $context onto which this operation should be applied
     * @return boolean TRUE if the operation can be applied onto the $context, FALSE otherwise
     */
    public function canEvaluate($context)
    {
        return (isset($context[0]) && ($context[0] instanceof NodeInterface) && $context[0]->getNodeType()->isOfType('ObisConcept.NeosProductsPlugin:Product'));
    }

    /**
     * {@inheritdoc}
     *
     * @param object $element
     * @param string $propertyPath
     * @return mixed
     */
    protected function getPropertyPath($element, $propertyPath)
    {
        switch ($propertyPath) {
            case 'categories':
                // this returns array of node identifiers of references and not the nodes itself
                return $element->getProperty($propertyPath, true);
                break;
        }

        return parent::getPropertyPath($element, $propertyPath);
    }

    /**
     * {@inheritdoc}
     *
     * @param mixed $value
     * @param string $operator
     * @param mixed $operand
     * @return boolean
     */
    protected function evaluateOperator($value, $operator, $operand)
    {
        if ($operator === '*=') {
            if (is_array($value)) {
                if (is_string($operand)) {
                    $operandValues = explode(',', $operand);
                    return count(array_intersect($value, $operandValues)) > 0;
                }
                return false;
            }
        }

        return parent::evaluateOperator($value, $operator, $operand);
    }
}
